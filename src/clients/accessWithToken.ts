import axios from 'axios'

export const accessWithToken = (accessToken : string) =>{
    return axios.get('http://localhost:8000/', { headers: { Authorization: `Bearer ${accessToken}` } })
    .then(
        result =>  {
        }
    ).catch(error => {
        throw new Error(error.response)
    })
}