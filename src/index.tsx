import React from 'react'
import ReactDOD from 'react-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App'


ReactDOD.render(
      <App />,
    document.getElementById('root')
)

