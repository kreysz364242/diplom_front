export default interface IUserInfo {
    user_id: string,
    exp : Number,
    iat : Number
    accessToken : string
}