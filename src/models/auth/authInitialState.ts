import IAuthState from './IAuthState'

export default {
    email: '',
    password: '',
    isAuthenticated : false
} as IAuthState