export default interface IInputValidComponent{
    isValid : boolean,
    feedBackText : string
}