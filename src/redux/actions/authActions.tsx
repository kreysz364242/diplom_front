import  IAction  from '../../models/IAction'

export const LOGGIN_IN = "LOGGIN_IN"
export const SET_AUTHINTICATE = "SET_AUTHINTICATE"
export const VERIFY_ACCESS_TOKEN = "VERIFY_ACCESS_TOKEN"
export const LOG_OUT = "LOG_OUT"


export const logOut = () : IAction => ({
    type: LOG_OUT
})

export const login = (email : string, password : string) : IAction => ({
    payload : {email, password},
    type: LOGGIN_IN
})


export const setAuthinticate = (isAuth : boolean) : IAction => ({
    payload: isAuth,
    type: SET_AUTHINTICATE
})

export const verifyAccessToken = () : IAction =>({
    type: VERIFY_ACCESS_TOKEN
})