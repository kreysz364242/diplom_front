import authInitialState from "../../models/auth/authInitialState";
import IAction from "../../models/IAction";
import IAuthState from '../../models/auth/IAuthState'

import {
    SET_AUTHINTICATE
} from '../actions/authActions'


export default function auth(
    state = authInitialState,
    {type , payload} : IAction

) : IAuthState {

    switch(type){
        case SET_AUTHINTICATE:{
            return { ...state, isAuthenticated: payload }
        }
    }
    return state
}