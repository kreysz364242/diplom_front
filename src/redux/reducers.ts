import { combineReducers } from "redux";
import { connectRouter } from 'connected-react-router'
import auth from './reducers/authReducer'


const createRootReducer = (history : any) => combineReducers({
    router: connectRouter(history),
    auth

  })
  export default createRootReducer