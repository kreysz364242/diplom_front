import { all } from 'redux-saga/effects'

import{
    watchRedirect,
    watchVerifyAccessToken,
    watchLogout
} from './saga/authSaga'



export default function* saga(){
    yield all(
        [
            watchRedirect(),
            watchVerifyAccessToken(),
            watchLogout()
        ]
    )
}