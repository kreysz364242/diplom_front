import { connect } from 'react-redux'
import IRootState from '../../models/rootState'
import Register from './register'

interface IAuthProps {
    email : string
    password : string
}


const mapDispatchToProps = (dispatch : any) =>{
    return{
        dispatch,
    }
}

const RegisterContainer = connect(
    mapDispatchToProps
)(Register)

export default RegisterContainer