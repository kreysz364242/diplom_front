import React from 'react'
import { Button } from 'reactstrap'

interface IHomeProps{
    isAuth: boolean
    dispatch(func: any) : void
    logOut(): void
} 

const Home = (props: IHomeProps) =>{
    return (
        <>
            <div>Home page</div>
            <Button outline color="danger" onClick={() => props.logOut()}>Logout</Button>
        </>
    )
    
}

export default Home