
import Home from './home'


import { connect } from 'react-redux'
import IRootState from '../../models/rootState'
import { logOut } from '../../redux/actions/authActions'

const mapStateToProps =  (state : IRootState) =>{
    return{
        isAuth: state.auth.isAuthenticated
    }
}



const mapDispatchToProps = (dispatch : any) =>{
    return{
        dispatch,
        logOut: ()  => dispatch(logOut()),
    }
}

const HomeContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)

export default HomeContainer