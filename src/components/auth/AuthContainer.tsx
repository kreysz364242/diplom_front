
import Auth from './Auth'

import { login } from '../../redux/actions/authActions'
import { connect } from 'react-redux'
import IRootState from '../../models/rootState'

const mapStateToProps =  (state : IRootState) =>{
    return{
        isAuth: state.auth.isAuthenticated
    }
}



const mapDispatchToProps = (dispatch : any) =>{
    return{
        dispatch,
        login: (email : string, password: string)  => dispatch(login(email, password)),
    }
}

const AuthContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Auth)

export default AuthContainer