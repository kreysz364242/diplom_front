import React, { useState, useEffect } from 'react'
import jwtDecode from 'jwt-decode';
import IRootState from '../../models/rootState'
import IAction from '../../models/IAction'
import '../../styles/login/login.scss'
import { push } from 'connected-react-router'
import IInputValidComponent from '../../models/auth/IInput'
import { Form, FormGroup, Label, Input, FormFeedback, Button, NavLink } from 'reactstrap';
import { checkEmailAndSetFeedBack, checkPasswordAndSetFeedBack } from '../../lib/chekInputAndSetFeedBack'
import axios from 'axios'
import IUserInfo from '../../models/auth/IUserInfo'
import { createFingerPrint } from '../../lib/createFingerprint'





interface IAuthProps {
    isAuth: boolean
    dispatch(func: any): any
    login(email: String, password: String): any
    downloadPage(): any
}


const Auth = (props: IAuthProps) => {
    const [userRegisterData, setUserRegisterData] = useState({ email: '', password: '', repeatPassword: '' })

    const [emailInputValid, setEmailInputValid] = useState({
        isValid: false,
        feedBackText: "Input your email!"
    } as IInputValidComponent)

    const [passwordInputValid, setPasswordInputValid] = useState({
        isValid: false,
        feedBackText: "Password can't be empty!"
    } as IInputValidComponent)



    const loginUser = () => {

        if (emailInputValid.isValid && passwordInputValid.isValid) {
            createFingerPrint().then(
                result =>{
                    console.log(result)
                    axios.post('http://localhost:8000/login', {
                user: {
                    email: userRegisterData.email,
                    password: userRegisterData.password,
                    fingerpath: result
                }
            })
                .then(
                    result => {
                        let userInfo: IUserInfo = {
                            ...jwtDecode(result.data.user.accessToken),
                            accessToken: result.data.user.accessToken
                        }
                        localStorage.setItem('userInfo', JSON.stringify(userInfo))
                        localStorage.setItem('refreshToken', JSON.stringify(result.data.user.refreshToken))
                        props.dispatch(push('/'))
                    }
                ).catch(error => {
                    console.log(error.response.data)
                })
                }
            )
            
        }
    }

    const setInputEmail = (email: string) => {
        setEmailInputValid(checkEmailAndSetFeedBack(email))
        setUserRegisterData({ ...userRegisterData, email })
    }

    const setInputPassword = (password: string) => {
        setPasswordInputValid(checkPasswordAndSetFeedBack(password))
        setUserRegisterData({ ...userRegisterData, password })
    }

    return (
        <>
            <div className="register">
                <p>Login on historicalPlacesTGN</p>
                <div className="register-input-area">
                    <Form onClick={e => e.preventDefault()}>
                        <FormGroup>
                            <Label for="email">Input your email</Label>
                            <Input
                                value={userRegisterData.email}
                                onChange={(event: any) => setInputEmail(event.target.value)}
                                valid={emailInputValid.isValid}
                                invalid={!emailInputValid.isValid}
                            />
                            <FormFeedback valid={emailInputValid.isValid}>{emailInputValid.feedBackText}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Label for="password">Input your password</Label>
                            <Input
                                type="password"
                                value={userRegisterData.password}
                                onChange={(event: any) => setInputPassword(event.target.value)}
                                valid={passwordInputValid.isValid}
                                invalid={!passwordInputValid.isValid}
                            />
                            <FormFeedback valid={passwordInputValid.isValid}>{passwordInputValid.feedBackText}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Button onClick={() => loginUser()} color="success">register</Button>
                            <NavLink href="#" onClick={() => props.dispatch(push('/register'))}>Register on historicalPlacesTGN</NavLink>
                        </FormGroup>

                    </Form>
                </div>
            </div>
        </>
    )

}

export default Auth