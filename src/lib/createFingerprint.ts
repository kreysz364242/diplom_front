import Fingerprint2 from 'fingerprintjs2'

export const createFingerPrint = () =>{
    const options = {
        excludes: {
          plugins: true,
          localStorage: true,
          adBlock: true,
          screenResolution: true,
          availableScreenResolution: true,
          enumerateDevices: true,
          pixelRatio: true,
          doNotTrack: true
        }
    }

    return new Promise((resolve, regect) =>{
        Fingerprint2.getPromise(options).then(
            components => {
                const values = components.map(component => component.value)
                resolve((Fingerprint2.x64hash128(values.join(''), 31)))
            }
        )
    })
    
}

