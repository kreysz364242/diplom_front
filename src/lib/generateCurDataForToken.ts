
export const generateDataForToken = () =>{
    const today = new Date();
    const expirationDate = new Date(today);   
    return parseInt((expirationDate.getTime() / 1000).toString(), 10)
}
