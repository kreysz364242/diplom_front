import IInputValidComponent from '../models/auth/IInput'

export const checkEmailAndSetFeedBack = (email : string) : IInputValidComponent =>{
    let regular = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    let newEmailInputValid: IInputValidComponent = {isValid : false, feedBackText: "Input your email!"} 

    email && regular.test(email) 
            ? newEmailInputValid = {isValid : true, feedBackText: "Success!"} 
            : newEmailInputValid = {isValid : false, feedBackText: "Email is incorrect"}
    
    return newEmailInputValid
}

export const checkPasswordAndSetFeedBack = (password: string) : IInputValidComponent =>{
    let newPasswordInputValid: IInputValidComponent = {isValid : false, feedBackText : "Password can't be empty!"}
    let regular = /[!@#$%^&*]/
    password && regular.test(password) 
        ? newPasswordInputValid = {isValid : false, feedBackText : "Password cannot contain characters !@#$%^&*]!"}
        : newPasswordInputValid = {isValid : true, feedBackText : "Success!"}
    
    return newPasswordInputValid
}